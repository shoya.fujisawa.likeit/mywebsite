CREATE DATABASE myec;
CREATE TABLE buy (
id          int PRIMARY KEY NOT NULL AUTO_INCREMENT,
user_id     int NOT NULL,
total_price varchar(255) NOT NULL,
buy_date    datetime NOT NULL
)

CREATE TABLE buy_detail (
id      int PRIMARY KEY NOT NULL AUTO_INCREMENT,
buy_id  int NOT NULL,
item_id int NOT NULL
)

CREATE TABLE item_category (
id            int NOT NULL AUTO_INCREMENT,
category_name varchar(255) NOT NULL
)

INSERT INTO item_category(
category_name
)VALUES(
'キッチン'
)
INSERT INTO item_category(
category_name
)VALUES(
'生活用品'
)
INSERT INTO item_category(
category_name
)VALUES(
'インテリア雑貨'
)
INSERT INTO item_category(
category_name
)VALUES(
'食器'
)
INSERT INTO item_category(
category_name
)VALUES(
'ファッション'
)
INSERT INTO item_category(
category_name
)VALUES(
'カゴ・手工芸品'
)
INSERT INTO item_category(
category_name
)VALUES(
'テーブル小物'
)

CREATE TABLE user (
id          int PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
email       varchar(255) NOT NULL UNIQUE,
name        varchar(255) NOT NULL,
code        varchar(255) NOT NULL,
address     varchar(255) NOT NULL,
password    varchar(255) NOT NULL,
create_date datetime NOT NULL,
update_date datetime NOT NULL
)
INSERT INTO user(
email,
name,
code,
address,
password,
create_date,
update_date
)VALUES(
'###111@gmail.com',
'管理者',
'0000000',
'東京都千代田区0-0-0',
'password',
now(),
now()
)

CREATE TABLE item(
id              int PRIMARY KEY NOT NULL AUTO_INCREMENT,
name            varchar(255) NOT NULL,
category_id     int NOT NULL,
price           int NOT NULL,
item_img_name   varchar(255) NOT NULL,
description     varchar(1000) NOT NULL,
scene_img_name  varchar(255) NOT NULL,
detail_img_name varchar(255) NOT NULL,
)
INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'炊飯用土鍋',
1,
6480,
'炊飯用土鍋.jpg',
'土鍋で炊くご飯ってチャレンジしたいけど、なかなか手が届かなかったところだと思います。けれど炊飯器と同じくらい簡単に、とってもおいしいご飯ができるなら鍋にしない手はありません。しかも、土鍋や文化鍋で炊くときに気をつけなきゃならないことは、全部やってくれる優れものなんです。全部やってくれる優れものなんです。鍋で炊くご飯に挑戦したかった方にはもちろん、既に鍋をお使いの方も、お手入れも楽なのでおすすめのアイテムですよ！',
'炊飯用土鍋scene.jpg',
'炊飯用土鍋詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'キャンドルウォーマーランプ',
3,
4320,
'キャンドルウォーマーランプ.jpg',
'慌ただしい1日を終えたあとは、リラックスして夜を過ごしたいもの。そんなとき、アロマの香りとやさしい灯りでリラックスできるランプの登場です。ハロゲンライトの熱でアロマキャンドルを溶かすから、火を使わずに香りが部屋に広がります。お気に入りのキャンドルをセットしておけば、帰ってすぐにスイッチをつけるだけで、癒やしの時間が手に入るんです。',
'キャンドルウォーマーランプscene.jpg',
'キャンドルウォーマーランプ詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'木目調の収納ラック',
3,
2000,
'木目調の収納ラック.jpg',
'キッチンや洗面台、食器棚、デスクなど、「高さが余ってもったいないな……」「もっと使いやすくしたいな……」と感じることはよくあるもの。そんなときに大活躍の「コの字型をしたラック」に、奥行きが短いスリムサイズが登場しました！',
'木目調の収納ラックscene.jpg',
'木目調の収納ラック詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'ベジタブルブラシ',
2,
3000,
'ベジタブルブラシ.jpg',
'スウェーデン生まれ、Iris（SRF） Hantverk社のブラシ達が届きました！100年以上にわたって地元スウェーデンで、また世界の国々で愛され続けてきた、まさに伝統ある暮らしの道具です。こちらは、ベジタブルブラシです。ブラシ部分は柔らかさが特徴の馬毛と、汚れをしっかりと取り除いてくれる植物繊維タンピコファイバーとの混合で作られているので、根菜の泥を落としたり、鍋底やフライパンを洗ったりと大変使い勝手の良いキッチン道具です。手で握った時の天然木ならではの気持ちよさや、人の手にフィットする持ちやすさも大きな魅力です。キッチンのタイルの上やステンレス台の上に置いた時の姿にも、味わい深さや温もりが感じられるので、キッチン周りのコーディネートにおいても存在感を発揮してくれるのではないかと思います。',
'ベジタブルブラシscene.jpg',
'ベジタブルブラシ詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'クッションカバー',
3,
5200,
'クッションカバー.jpg',
'スウェーデンのテキスタイルメーカー「KLIPPANクリッパン」の新作クッションカバー。風がすーっと吹き抜けるような、爽やかな印象のデザインだから、ちょっと雰囲気を変えたいなという時にピッタリ。',
'クッションカバーscene.jpg',
'クッションカバー詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'スタッキングワイングラス',
4,
540,
'スタッキングワイングラス.jpg',
'グラス製造の技術を世界的に評価されている「Libbey（リビー）」社のヨーロッパブランド「royal leerdam（ロイヤル レアダム）」。手に取りやすく、食卓を上質に彩ってくれる脚付きデザインのグラスは、毎日使いたい日用品となりそうです。',
'スタッキングワイングラスscene.jpg',
'スタッキングワイングラス詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'ステンレスケトル',
1,
11880,
'ステンレスケトル.jpg',
'錆びにくく耐久性の強さも魅力なOPA社のステンレス。そんな素材を活かしたシンプルなデザインは、キッチンの空間にすんなり馴染んでくれます。こちらのケトルはかわいらしいコンパクトサイズで、女性も片手で楽にそそぐことができます。蓋がしっかりとしまるつくりになっているので、注いでいるあいだにコロンと落ちてしまう心配もありませんよ。',
'ステンレスケトルscene.jpg',
'ステンレスケトル詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'トートバッグ',
5,
3780,
'トートバッグ.jpg',
'上質な素材で作られたテキスタイルが人気の、北欧のブランドLAPUAN KANKURIT（ラプアン・カンクリ）から、コットンリネンのトートバッグが届きました！',
'トートバッグscene.jpg',
'トートバッグ詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'マグネットフック',
2,
280,
'マグネットフック.jpg',
'暮らしのモヤモヤを解決してくれる、小技の効いた収納アイテム見つけました。今回ご紹介する「マグネットフック」は、強力な磁石で掛けるものを選ばず、小さいからこそどんなところにも付けられる優れもの！その便利さから、当店のスタッフにも愛用者の多いアイテムなんです。シンプルな見た目はどんな空間にもなじみ、アイデア次第で様々な使い方ができるので、限られたお家のスペースに「新たな収納」を生み出してくれますよ。',
'マグネットフックscene.jpg',
'マグネットフック詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'レザーシューズ',
5,
21600,
'レザーシューズ.jpg',
'"長く愛着の持てる靴" をコンセプトに掲げるシューズブランドchausser（ショセ）より、 革靴なのに、スニーカー感覚ではけて歩きやすい「レザーシューズ」をご紹介します。きちんとした印象でありながら、インソールが柔らかく、とにかく歩きやすいのがポイント。しかも防水加工済みで、急な雨も安心という頼もしいアイテムなんです。たくさん歩くけど、オシャレも諦めたくない日の、頼れる一足となってくれるはず。',
'レザーシューズscene.jpg',
'レザーシューズ詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'レンジで温められる一膳おひつ',
1,
1900,
'レンジで温められる一膳おひつ.jpg',
'ごはんが一層おいしくなる、新米の季節。炊きたては格別だけど、冷蔵保存したものでやりくりする日もあります。当然、チンしたごはんの食感・風味が落ちるのはしょうがないと思っていました。だけどこのおひつを使うと「昨日、一昨日のごはんがこんなにもふっくら、もちもちのままなんだ！」と感動します。 耐熱・冷蔵OKでコンパクト。とにかく使いやすく、手間いらずの「一膳おひつ」をご紹介します。',
'レンジで温められる一膳おひつscene.jpg',
'レンジで温められる一膳おひつ詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'玄関マット',
3,
5940,
'玄関マット.jpg',
'玄関のインテリアがすてきな家って、憧れです。お呼ばれしたお宅で、気が利いたデザインのスリッパや玄関マットがあるだけで気分があがります。けれど、ついリビングやダイニングに比べて、インテリアを整えるのを後回しにしがち。私たちのそんな思いから、オリジナルで玄関マットを作りました！大きくインテリアを変えなくても、玄関マットを敷くだけで 、玄関の雰囲気はパッと変わります。',
'玄関マットscene.jpg',
'玄関マット詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'国産帆布の折りたたみイス',
3,
5940,
'国産帆布の折りたたみイス.jpg',
'インテリアにすっとなじみ、暮らしに合わせて活躍してくれる便利な小家具・折りたたみイスのご紹介です。アウトドアでよく見かける形ですが、座面には当店別注カラーの帆布を、脚にはビーチ材を使用したシックな造りなんです。林業の盛んな福岡県・うきは市で古くから木製家具を作り続けている「杉工場」が手がける、こだわりの小家具。お部屋の中にひとつあるだけでちょっとした悩みを解決してくれる、心強いアイテムですよ。',
'国産帆布の折りたたみイスscene.jpg',
'国産帆布の折りたたみイス詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'折りたたみ式ローテーブル',
3,
3240,
'折りたたみ式ローテーブル.jpg',
'ピクニックやBBQなどアウトドアが楽しくなる季節。そんなお出かけにピッタリな「折り畳んで持ち運びのできる」ローテーブルをご紹介します。シートの上でお弁当を広げるときも、こぼれるのが心配なドリンク類はちょっとしたテーブルがあると便利♪また、室内で使うのにもちょうど良いサイズ感ですよ。',
'折りたたみ式ローテーブルscene.jpg',
'折りたたみ式ローテーブル詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'箸(山吹色)',
4,
1080,
'箸(山吹色).jpg',
'お箸は日々使うもの。そのためお気に入りを見つけられたら、暮らしがもっと楽しくなるかもしれません。私たちの日々の食卓には、煮物もあればハンバーグも並びます。毎日使うなら、そのどちらにも似合うお箸が欲しい。そこで見つけたのが、「にっぽん伝統色箸」でした。鮮やかだけれど馴染みの良い雰囲気は、和食だけでなく当店で扱っている北欧の食器にもぴったり。',
'箸(山吹色)scene.jpg',
'箸(山吹色)詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'箸置き',
7,
4050,
'箸置き.jpg',
'忙しい毎日の中で、ほっと一息つける食事の時間は大事にしたい。けれど、ついつい早く食べてしまって「昨日のご飯の味ちゃんと覚えてないな…」と思うこともしばしばあります。たとえば「箸置き」が食卓にあることで、お箸を休める時間が増えたなら、毎日の食事をゆったり楽しむきっかけになるかもしれません。今回ご紹介するのは、作家・nakagawa kumiko（なかがわ くみこ）さんの手がけた箸置き。錫（すず）素材と花モチーフで美しい見た目は、いつもの食卓を少し大人っぽく上質な雰囲気にしてくれます。',
'箸置きscene.jpg',
'箸置き詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'壁掛け時計',
3,
10800,
'壁掛け時計.jpg',
'暮らしの中で、毎日目にする掛け時計。インテリアの印象を決めるものだからこそ、買い替えも慎重になりがちです。できることならデザインにも機能にも、納得がいくものに巡り合いたい。今回ご紹介する「Campagne（カンパーニュ）」は、そんな希望を叶える時計。店長佐藤も自宅で愛用するアイテムなんです！',
'壁掛け時計scene.jpg',
'壁掛け時計詳細.png'
)

INSERT INTO item(
name,
category_id,
price,
item_img_name,
description,
scene_img_name,
detail_img_name
)VALUES(
'長財布',
6,
19440,
'長財布.jpg',
'兵庫県にあるセレクトショップPermanent Age（パーマネントエイジ）より、長財布をご紹介します。見た目はとってもシンプルですが、厚みのあるデザインのため、その分収納力がすごいんです！「こんなお財布探していた」と、思わず嬉しくなってしまう機能が満載ですよ。',
'長財布scene.jpg',
'長財布詳細.png'
)

CREATE TABLE cart(
  id      int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  item_id int NOT NULL,
  user_id int NOT NULL
)

CREATE TABLE buy_history(
  id       int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  item_id  int NOT NULL,
  buy_id   int
)
