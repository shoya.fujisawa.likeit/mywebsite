<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta http-equiv="charset=UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/original/top.css" rel="stylesheet">
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <script src="https://kit.fontawesome.com/f6a1a0c1f9.js"></script>
   <title>購入内容確認画面</title>
</head>

<body>
   <div class="title">
      <a href="top">
         <img class="logo" src="image/logo.jpg">
      </a>
   </div>
   <div class="container">
   <div class="row">
   <div class="col-md-6 offset-md-3">
   <i class="fas fa-search"></i>
   <div class="input-group">
	 <input type="text" class="form-control" placeholder="キーワードを入力">
	 <span class="input-group-btn">
		<input type="submit" class="btn btn-outline-warning" value="検索">
	 </span>
   </div>
   </div>
   <div class="col-md-2 offset-md-5">
      <p class="cart_title text-center">購入内容確認</p>
   </div>
   <div class="col-md-10 offset-md-1">
      <table class="table">
         <thead class="thead-light">
            <tr>
               <th>商品名</th>
               <th>価格</th>
               <th>小計</th>
            </tr>
         </thead>
         <tbody>
               <c:forEach var="item" items="${sessionScope.itemList}">
               <tr>
                  <td>${item.name}</td>
                  <td>${item.formatPrice}円(税込)</td>
                  <td>${item.formatPrice}円</td>
               </tr>
               </c:forEach>
               <tr>
                  <td>送料</td>
                  <td></td>
                  <td>350円
               </tr>

               <tr>
                  <td></td>
                  <td>合計金額</td>
                  <td>${sessionScope.tp}円</td>
               </tr>

         </tbody>
      </table>
   </div>
   <div class="offset-md-4">
      <a href="cart2">
         <button type="button" class="btn btn-outline-secondary">戻る</button>
      </a>
   </div>
   <div class="col-md-2 offset-md-3">
      <a href="buy">
         <button type="submit" class="btn btn-outline-success">購入完了</button>
      </a>
   </div>
   </div>
   </div>
</body>
</html>