<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta http-equiv="charset=UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/original/top.css" rel="stylesheet">
   <link href="css/original/login.css" rel="stylesheet">
   <link href="css/bootstrap.min.css" rel="stylesheet">
<title>ログイン画面</title>
</head>

<body>
   <div class="title">
      <a href="top">
         <img class="logo" src="image/logo.jpg">
      </a>
   </div>

   <div class="row login">
     <div class="mx-auto" style="width:800px;">
      <h1>ログイン</h1>
      <p style="text-align:center"><span style="color:#ff0000;">${requestScope.errMsg}</span></p>
      <form action="Login" method="post">
         <div class="form col-md-6 offset-md-3">
            <label for="input_mail">メールアドレス：<input type="text" id="mail_address" name="mail_address" placeholder="メールアドレス" value="${requestScope.email}"/></label>
         </div>

         <div class="form col-md-6 offset-md-3">
            <label for="input_password">パスワード：　　<input type="password" id="password" name="password" placeholder="パスワード" value="${requestScope.password}"/></label>
         </div>


         <div class="form col-md-4 offset-md-4">
           <input type="submit" class="btn btn-primary btn-block btn-warning" value="ログイン">
         </div>
      </form>
     </div>
   </div>
   <br>
   <br>
   <hr>
   <br>
   <br>
   <div class="row user-create">
     <div class="mx-auto" style="width:800px;">
       <h1>新規登録</h1>
       <p style="text-align:center"><span style="color:#ff0000;">${requestScope.errMsg2}</span></p>
       <form action="userCreate" method="post">
       　<div class="form col-md-6 offset-md-3">
            <label for="input_password">名前：　　　　　　<input type="text" name="name" id="name" placeholder="名前" value="${requestScope.userName}"/></label>
         </div>

         <div class="form col-md-6 offset-md-3">
            <label for="input_mail">メールアドレス：　<input type="text" name="mail_address" id="mail_address" placeholder="メールアドレス"/></label>
         </div>

         <div class="form col-md-6 offset-md-3">
            <label for="input_password">パスワード：　　　<input type="password" name="password" id="password" placeholder="パスワード"/></label>
         </div>

         <div class="form col-md-6 offset-md-3">
            <label for="input_password">パスワード(確認)： <input type="password" name="password-confirm" id="password-confirm" placeholder="パスワード(確認)"/></label>
         </div>

         <div class="form col-md-6 offset-md-3">
            <label for="input_password">郵便番号： 　　　  <input type="text" name="code" id="code" placeholder="郵便番号(ハイフンなし)" value="${requestScope.userCode}"/></label>
         </div>

         <div class="form col-md-6 offset-md-3">
            <label for="input_password">住所： <input type="text" name="address" id="address" placeholder="住所" value="${requestScope.userAddress}"/></label>
         </div>

         <div class="form col-md-4 offset-md-4">
           <input type="submit" class="btn btn-primary btn-block btn-warning" value="新規登録">
         </div>
       </form>
       <div class="text-center">
       <br>
       <br>
       <a href="top">
         <button type="button" class="btn btn-outline-secondary">戻る</button>
       </a>
       <br>
       <br>
       </div>
     </div>
   </div>
</body>
</html>