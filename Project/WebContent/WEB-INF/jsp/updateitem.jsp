<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta http-equiv="charset=UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/original/top.css" rel="stylesheet">
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <script src="https://kit.fontawesome.com/f6a1a0c1f9.js"></script>
   <title>商品情報更新画面</title>
</head>

<body>
   <div class="title">
      <a href="top">
         <img class="logo" src="image/logo.jpg">
      </a>
   </div>

   <div class="container">
   <div class="row">
   <div class="col-md-6 offset-md-3">
   <i class="fas fa-search"></i>
   <div class="input-group">
	 <input type="text" class="form-control" placeholder="キーワードを入力">
	 <span class="input-group-btn">
	    <a href="itemlist">
		<input type="submit" class="btn btn-outline-warning" value="検索">
		</a>
	 </span>
   </div>
   <div class="row">
      <div class="col-md-4 offset-md-4">
         <br>
         <p class="text-center">商品情報更新</p>
         <br>
      </div>
   </div>
   <form action="updateitem?id=${requestScope.itemList.id}" method="post">
   <div class="row form-group">
      <div class="col-md-3">
         <p>商品名：</p>
      </div>
      <div class="offset-md-3">
         <input type="text" id="item_name" name="item_name" value="${requestScope.itemList.name}"/>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-3 text-nowrap">
         <p>カテゴリー番号：</p>
      </div>
      <div class="offset-md-3">
         <input type="text" id="category_id" name="category_id" value="${requestScope.itemList.category_id}"/>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-3">
         <p>価格：</p>
      </div>
      <div class="offset-md-3">
         <input type="text" id="price" name="price" value="${requestScope.itemList.price}"/>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-4">
         <p>商品画像：</p>
      </div>
      <div class="col-md-4 offset-md-2">
         <input type="text" id="item_pic" name="item_pic" value="${requestScope.itemList.item_img_name}"/>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-3 text-nowrap">
         <p>商品説明：</p>
      </div>
      <div class="offset-md-3">
         <textarea name="item_description" rows="4" cols="30">${requestScope.itemList.description}</textarea>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-5">
         <p>使用シーン画像：</p>
      </div>
      <div class="col-md-4 offset-md-1">
         <input type="text" id="itemscene_pic" name="itemscene_pic" value="${requestScope.itemList.scene_img_name}"/>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-5">
         <p>商品詳細画像：</p>
      </div>
      <div class="col-md-4 offset-md-1">
         <input type="text" id="itemdetail_pic" name="itemdetail_pic" value="${requestScope.itemList.detail_img_name}"/>
      </div>
   </div>
   <div class="row form-group updateitemButton">
      <div class="offset-md-3">
      <a href="item?id=${requestScope.itemList.id}">
         <button type="button" class="btn btn-outline-secondary">戻る</button>
      </a>
      </div>
      <div class="offset-md-4">

         <button type="submit" class="btn btn-outline-success">商品情報を更新</button>

      </div>
   </div>
   </form>
   </div>
   </div>
   </div>
</body>
</html>