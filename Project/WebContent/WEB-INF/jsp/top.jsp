<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/original/top.css" rel="stylesheet">
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <script src="https://kit.fontawesome.com/f6a1a0c1f9.js"></script>
   <title>トップページ</title>
</head>
<body>
   <div class="title">
      <a href="top">
         <img class="logo" src="image/logo.jpg">
      </a>
   </div>

   <div class="container">
   <div class="row">

   <div class="col-md-6 offset-md-3">
   <i class="fas fa-search"></i>
   <form action="top" method="post">
   <div class="input-group">
	 <input type="text" class="form-control" name="searchword" placeholder="商品名を入力">
	 <span class="input-group-btn">
		<input type="submit" class="btn btn-outline-warning" value="検索">
	 </span>
   </div>
   </form>
   </div>

   <div class="col-md-3 offset-md-3">
      <img class="topimage" src="image/topimage.jpg">
   </div>
   <div class="col-md-1 offset-md-3 side-menu">
      <ul class="side-menu">
        <li>
        <c:if test="${sessionScope.userInfo==null}">
        <a href="Login">
           <img class="login" src="image/ログイン.jpg">
        </a>
        </c:if>
        <c:if test="${sessionScope.userInfo!=null}">
        <a href="logout">
           <img class="logout" src="image/ログアウト.jpg">
        </a>
        </c:if>
        </li>
        <br>
        <c:if test="${sessionScope.userInfo!=null}">
        <li>
        <a href="userPage">
           <img class="userpage" src="image/マイページ.jpg">
        </a>
        </li>
        </c:if>
        <br>
        <c:if test="${sessionScope.userInfo.id==1}">
        <li>
        <a href="additem">
           <img class="additem" src="image/商品管理.jpg">
        </a>
        </li>
        </c:if>
        <br>
        <li>
        <a href="cart2">
           <img class="cart" src="image/カート.jpg">
        </a>
        </li>

        <li>
        <p class="text-nowrap">カテゴリーから探す</p>
        </li>
        <li>
        <a href="itemSearchResult?id=1">
           <img src="image/キッチン.jpg">
        </a>
        </li>
        <br>
        <li>
        <a href="itemSearchResult?id=2">
           <img src="image/生活日用品.jpg">
        </a>
        </li>
        <br>
        <li>
        <a href="itemSearchResult?id=3">
           <img src="image/インテリア雑貨.jpg">
        </a>
        </li>
        <br>
        <li>
        <a href="itemSearchResult?id=4">
           <img src="image/食器.jpg">
        </a>
        </li>
        <br>
        <li>
        <a href="itemSearchResult?id=5">
           <img src="image/ファッション.jpg">
        </a>
        </li>
        <br>
        <li>
        <a href="itemSearchResult?id=6">
           <img src="image/カゴ・手工芸品.jpg">
        </a>
        </li>
        <br>
        <li>
        <a href="itemSearchResult?id=7">
           <img src="image/テーブル小物.jpg">
        </a>
        </li>
      </ul>
   </div>
   <!--  <div class="col-md-3 offset-md-3">
      <img class="topimage" src="/image/topimage.jpg">
   </div>-->
   </div>
   <br>

   <br>
   <br>

   <p class="text-center">おすすめ商品</p>
   <br>
   <div class="favorite-area">
    <div class="row">
	   <c:forEach var="item" items="${requestScope.itemList}">

		   <div class="col-md-6">
		      <a href="item?id=${item.id}">
		      <img class="item" src="image/${item.item_img_name}">
		      </a>
		      <p>${item.name}<p>
		      <p>${item.formatPrice}円(税込)</p>
		   </div>
	   </c:forEach>
  	 </div>
   </div>
   </div>
</body>
</html>