<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta http-equiv="charset=UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/original/top.css" rel="stylesheet">
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <script src="https://kit.fontawesome.com/f6a1a0c1f9.js"></script>
   <title>ユーザー情報更新画面</title>
</head>

<body>
   <div class="title">
      <a href="top">
         <img class="logo" src="image/logo.jpg">
      </a>
   </div>

   <div class="container">
   <div class="row">
   <div class="col-md-6 offset-md-3">
   <i class="fas fa-search"></i>
   <div class="input-group">
	 <input type="text" class="form-control" placeholder="キーワードを入力">
	 <span class="input-group-btn">
	    <a href="itemlist.html">
		<input type="submit" class="btn btn-outline-warning" value="検索">
		</a>
	 </span>
   </div>
   <div class="row">
      <div class="col-md-5 offset-md-4">
         <br>
         <p>ユーザー情報更新</p>
      </div>
   </div>
   <br>
   <form action="userUpdate?id=${sessionScope.userInfo.id}" method="post">
   <div class="row form-group">
      <div class="col-md-3">
      <p>名前：</p>
      </div>
      <div class="offset-md-2">
      <p>${requestScope.UserInfo.name}</p>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-2 text-nowrap">
      <p>メールアドレス：</p>
      </div>
      <div class="offset-md-3">
      <input type="text" id="mail_address" name="mail_address" value="${requestScope.UserInfo.email}"/>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-2 text-nowrap">
      <p>郵便番号：</p>
      </div>
      <div class="offset-md-3">
      <input type="text" id="code" name="code" value="${requestScope.UserInfo.code}"/>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-2 text-nowrap">
      <p>住所：</p>
      </div>
      <div class="offset-md-3">
      <input type="text" id="address" name="address" value="${requestScope.UserInfo.address}"/>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-2 text-nowrap">
      <p>パスワード：</p>
      </div>
      <div class="offset-md-3">
      <input type="password" id="password" name="password" placeholder="パスワード"/>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-2 text-nowrap">
      <p>パスワード(確認)：</p>
      </div>
      <div class="offset-md-3">
      <input type="password" id="password-confirm" name="password-confirm" placeholder="パスワード(確認)"/>
      </div>
   </div>
   <div class="row">
      <div class="offset-md-3">
      <a href="userPage">
         <button type="button" class="btn btn-outline-secondary">戻る</button>
      </a>
      </div>
      <div class="col-md-3 offset-md-3">
         <input type="submit" class="btn btn-primary btn-block btn-warning" value="更新">
      </div>
   </div>
   </form>
   </div>
   </div>
   </div>
</body>
</html>