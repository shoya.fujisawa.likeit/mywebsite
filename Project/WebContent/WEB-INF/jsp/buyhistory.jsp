<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta http-equiv="charset=UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/original/top.css" rel="stylesheet">
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <script src="https://kit.fontawesome.com/f6a1a0c1f9.js"></script>
   <title>購入履歴画面</title>
</head>

<body>
   <div class="title">
      <a href="top">
         <img class="logo" src="image/logo.jpg">
      </a>
   </div>

   <div class="container">
   <div class="row">
   <div class="col-md-6 offset-md-3">
   <i class="fas fa-search"></i>
   <div class="input-group">
	 <input type="text" class="form-control" placeholder="キーワードを入力">
	 <span class="input-group-btn">
	    <a href="itemlist.html">
		<input type="submit" class="btn btn-outline-warning" value="検索">
		</a>
	 </span>
   </div>
   </div>
      <div class="buyhistory col-md-8 offset-md-2">
      <div>
         <br>
         <p class=text-center>購入履歴</p>
      </div>
      <div class="col-md-12">
         <table class="table">
            <thead class="thead-light">
               <tr>
                  <th>購入日</th>
                  <th>商品名</th>
                  <th>価格</th>
                  <th></th>
               </tr>
            </thead>
            <tbody>
               <c:forEach var="buyData" items="${requestScope.buyData}">
               <tr>
                  <td>${buyData.formatDate}</td>
                  <td>${buyData.name}</td>
                  <td>${buyData.formatPrice}円</td>
                  <td><a href="item?id=${buyData.item_id}"><button type="button" class="btn btn-outline-info">商品詳細</button></a></td>
               </tr>
               </c:forEach>
            </tbody>
         </table>
      </div>
      <div class=row>
      <div class="col-md-2 offset-md-3">
         <a href="userPage">
            <button type="button" class="btn btn-outline-secondary">戻る</button>
         </a>
      </div>
      <div class="offset-md-2">
         <a href="top">
            <button type="button" class="btn btn-outline-success">トップページへ</button>
         </a>
      </div>
      </div>
      </div>
   </div>
   </div>
   <br>
   <br>
</body>
</html>