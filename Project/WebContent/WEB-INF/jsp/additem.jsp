<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta http-equiv="charset=UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/original/top.css" rel="stylesheet">
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <script src="https://kit.fontawesome.com/f6a1a0c1f9.js"></script>
   <title>商品追加画面</title>
</head>

<body>
   <div class="title">
      <a href="top">
         <img class="logo" src="image/logo.jpg">
      </a>
   </div>

   <div class="container">
   <div class="row">
   <div class="col-md-6 offset-md-3">
   <i class="fas fa-search"></i>
   <div class="input-group">
	 <input type="text" class="form-control" placeholder="キーワードを入力">
	 <span class="input-group-btn">
	    <a href="itemlist.html">
		<input type="submit" class="btn btn-outline-warning" value="検索">
		</a>
	 </span>
   </div>
   <div class="row">
      <div class="col-md-4 offset-md-4">
         <br>
         <p class="text-center">商品を追加</p>
         <br>
      </div>
   </div>
   <div class="row">
      <div class="offset-md-4">
         <p><span style="color:#ff0000;">${requestScope.errMsg}</span></p>
      </div>
   </div>
   <form action="additem" enctype=multipart/form-data method="post">
   <div class="row form-group">
      <div class="col-md-3">
         <p>商品名：</p>
      </div>
      <div class="offset-md-3">
         <input type="text" id="item_name" name="item_name" placeholder="商品名"/>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-3 text-nowrap">
         <p>カテゴリー番号：</p>
      </div>
      <div class="offset-md-3">
         <select name="category_id" size="1">
         <option value="1" label="1.キッチン">
         <option value="2" label="2.生活日用品">
         <option value="3" label="3.インテリア雑貨">
         <option value="4" label="4.食器">
         <option value="5" label="5.ファッション">
         <option value="6" label="6.カゴ・手工芸品">
         <option value="7" label="7.テーブル小物">
         </select>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-3">
         <p>価格：</p>
      </div>
      <div class="offset-md-3">
         <input type="text" id="price" name="price" placeholder="価格"/>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-4 text-nowrap">
         <p>商品画像ファイル名：</p>
      </div>
      <div class="col-md-4 offset-md-2">
         <input type="file" id="item_pic" name="item_pic" placeholder="商品画像"/>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-3 text-nowrap">
         <p>商品説明：</p>
      </div>
      <div class="offset-md-3">
         <textarea name="item_description" rows="4" cols="30" placeholder="商品説明"></textarea>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-5 text-nowrap">
         <p>使用シーン画像ファイル名：</p>
      </div>
      <div class="col-md-4 offset-md-2">
         <input type="file" id="itemscene_pic" name="itemscene_pic" placeholder="使用シーン画像"/>
      </div>
   </div>
   <div class="row form-group">
      <div class="col-md-5 text-nowrap">
         <p>商品詳細画像ファイル名：</p>
      </div>
      <div class="col-md-4 offset-md-2">
         <input type="file" id="itemdetail_pic" name="itemdetail_pic" placeholder="商品詳細画像"/>
      </div>
   </div>
   <div class="row form-group additemButton">
      <div class="offset-md-3">
      <a href="top.html">
         <button type="button" class="btn btn-outline-secondary">戻る</button>
      </a>
      </div>
      <div class="offset-md-4">
      <a href="top.html">
         <button type="submit" class="btn btn-outline-success">商品を登録</button>
      </a>
      </div>
   </div>
   </form>
   </div>
   </div>
   </div>
</body>
</html>