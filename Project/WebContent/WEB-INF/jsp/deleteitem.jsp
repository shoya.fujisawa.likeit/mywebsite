<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta http-equiv="charset=UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/original/top.css" rel="stylesheet">
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <script src="https://kit.fontawesome.com/f6a1a0c1f9.js"></script>
   <title>商品情報削除画面</title>
</head>

<body>
   <div class="title">
      <a href="top.html">
         <img class="logo" src="image/logo.jpg">
      </a>
   </div>

   <div class="container">
   <div class="row">
   <div class="col-md-6 offset-md-3">
   <i class="fas fa-search"></i>
   <div class="input-group">
	 <input type="text" class="form-control" placeholder="キーワードを入力">
	 <span class="input-group-btn">
	    <a href="itemlist.html">
		<input type="submit" class="btn btn-outline-warning" value="検索">
		</a>
	 </span>
   </div>
   <div class="row">
      <div class="col-md-5 offset-md-4">
         <br>
         <p class="text-center">商品情報を削除</p>
         <br>
      </div>
   </div>
   <div class="row">
      <div class="col-md-10 offset-md-1">
         <br>
         <p class="text-center text-nowrap">${requestScope.itemList.name} に関する商品情報を削除しますか？</p>
      </div>
   </div>
   <form method="post" action="deleteitem?id=${requestScope.itemList.id}">
   <div class="row">

      <div class="offset-md-3">
      <br>
      <a href="item?id=${requestScope.itemList.id}">
         <button type="button" class="btn btn-outline-success">いいえ</button>
      </a>
      </div>
      <div class="offset-md-3">
      <br>
      <button type="submit" class="btn btn-outline-danger">はい</button>
      </div>

   </div>
   </form>
   </div>
   </div>
   </div>
</body>
</html>