<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta http-equiv="charset=UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/original/top.css" rel="stylesheet">
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <script src="https://kit.fontawesome.com/f6a1a0c1f9.js"></script>
   <title>マイページ画面</title>
</head>

<body>
   <div class="title">
      <a href="top">
         <img class="logo" src="image/logo.jpg">
      </a>
   </div>

   <div class="container">
   <div class="row">
   <div class="col-md-6 offset-md-3">
   <i class="fas fa-search"></i>
   <div class="input-group">
	 <input type="text" class="form-control" placeholder="キーワードを入力">
	 <span class="input-group-btn">
	    <a href="itemlist.html">
		<input type="submit" class="btn btn-outline-warning" value="検索">
		</a>
	 </span>
   </div>
   <div class="row">
      <div class="col-md-4 offset-md-4">
         <br>
         <p>マイページ</p>
      </div>
   </div>
   <br>
   <div class="row">
      <div class="col-md-3">
      <p>名前：</p>
      </div>
      <div class="offset-md-2">
      <p>${requestScope.UserInfo.name}</p>
      </div>
   </div>
   <br>
   <div class="row">
      <div class="col-md-2 text-nowrap">
      <p>メールアドレス：</p>
      </div>
      <div class="offset-md-3">
      <p>${requestScope.UserInfo.email}</p>
      </div>
   </div>
   <br>
   <div class="row">
      <div class="col-md-2 text-nowrap">
      <p>郵便番号：</p>
      </div>
      <div class="offset-md-3">
      <p>${requestScope.UserInfo.code}</p>
      </div>
   </div>
   <br>
   <div class="row">
      <div class="col-md-3">
      <p>住所：</p>
      </div>
      <div class="offset-md-2">
      <p>${requestScope.UserInfo.address}</p>
      </div>
   </div>
   <br>
   <div class="row">
      <div class="col-md-2 text-nowrap">
      <p>登録日：</p>
      </div>
      <div class="offset-md-3">
      <p>${requestScope.UserInfo.formatDate}</p>
      </div>
   </div>
   <br>
   <br>
   <div class="row">
      <div class="col-md-4">
      <a href="buyhistory">
         <button type="submit" class="btn btn-outline-success">購入履歴を見る</button>
      </a>
      </div>
      <div class="offset-md-4">
      <a href="userUpdate?id=${sessionScope.userInfo.id}">
         <button type="button" class="btn btn-outline-info">登録情報の変更</button>
      </a>
      </div>
   </div>
   </div>

   <div class="col-md-1 side-menu">
      <ul class="side-menu">
        <li>
        <c:if test="${sessionScope.userInfo.name==null}">
        <a href="Login">
           <img class="login" src="image/ログイン.jpg">
        </a>
        </c:if>
        <c:if test="${sessionScope.userInfo.name!=null}">
        <a href="logout">
           <img class="logout" src="image/ログアウト.jpg">
        </a>
        </c:if>
        </li>
        <br>
        <c:if test="${sessionScope.userInfo.name!=null}">
        <li>
        <a href="userPage">
           <img class="userpage" src="image/マイページ.jpg">
        </a>
        </li>
        </c:if>
        <br>
        <c:if test="${sessionScope.userInfo.name=='管理者'}">
        <li>
        <a href="additem">
           <img class="additem" src="image/商品管理.jpg">
        </a>
        </li>
        </c:if>
        <br>
        <li>
        <a href="cart2">
           <img class="cart" src="image/カート.jpg">
        </a>
        </li>
        <li>
        <p class="text-nowrap">カテゴリーから探す</p>
        </li>
        <li>
        <a href="itemSearchResult?id=1">
           <img src="image/キッチン.jpg">
        </a>
        </li>
        <br>
        <li>
        <a href="itemSearchResult?id=2">
           <img src="image/生活日用品.jpg">
        </a>
        </li>
        <br>
        <li>
        <a href="itemSearchResult?id=3">
           <img src="image/インテリア雑貨.jpg">
        </a>
        </li>
        <br>
        <li>
        <a href="itemSearchResult?id=4">
           <img src="image/食器.jpg">
        </a>
        </li>
        <br>
        <li>
        <a href="itemSearchResult?id=5">
           <img src="image/ファッション.jpg">
        </a>
        </li>
        <br>
        <li>
        <a href="itemSearchResult?id=6">
           <img src="image/カゴ・手工芸品.jpg">
        </a>
        </li>
        <br>
        <li>
        <a href="itemSearchResult?id=7">
           <img src="image/テーブル小物.jpg">
        </a>
        </li>
      </ul>
   </div>
   </div>
   </div>
</body>
</html>