package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.BuyDataBeans;

public class BuyDataDao {
	public BuyDataBeans getBuyData(int buy_id) {
		Connection con=null;
		try {
			con=DBManager.getConnection();

			String sql="SELECT*FROM buy WHERE id=?";
			PreparedStatement st=con.prepareStatement(sql);

			st.setInt(1, buy_id);
			ResultSet rs=st.executeQuery();
			if(!rs.next()) {
				return null;
			}

			BuyDataBeans bdb=new BuyDataBeans();
			bdb.setTotal_price(rs.getString("total_price"));
			bdb.setBuy_date(rs.getDate("buy_date"));

			return bdb;

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public ArrayList<BuyDataBeans> getBuyDataDetail(int buy_id) {
		Connection con=null;
		try {
			con=DBManager.getConnection();

			String sql="SELECT name,price "
					  +"FROM item "
					  +"JOIN buy_history "
					  +"ON item.id=buy_history.item_id "
					  +"WHERE buy_id=?";
			PreparedStatement st=con.prepareStatement(sql);
			st.setInt(1, buy_id);

			ResultSet rs=st.executeQuery();
			ArrayList<BuyDataBeans> BuyData=new ArrayList<BuyDataBeans>();

			while(rs.next()) {
				BuyDataBeans bdb=new BuyDataBeans();
				bdb.setName(rs.getString("name"));
				bdb.setPrice(rs.getInt("price"));

				BuyData.add(bdb);
			}
			return BuyData;
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
