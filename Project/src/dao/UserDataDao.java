package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

import beans.UserDataBeans;

public class UserDataDao {
	public UserDataBeans logininfo(String email,String password) {
		Connection con=null;
		try {
			con=DBManager.getConnection();
			String source=password;
			Charset charset=StandardCharsets.UTF_8;
			String algorithm="MD5";

			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String pass = DatatypeConverter.printHexBinary(bytes);
			String sql="SELECT*FROM user WHERE email=? and password=?";

			PreparedStatement st=con.prepareStatement(sql);
			st.setString(1, email);
			st.setString(2, pass);
			ResultSet rs=st.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int id=rs.getInt("id");
			String emailData=rs.getString("email");
			String nameData=rs.getString("name");
			String codeData=rs.getString("code");
			String addressData=rs.getString("address");
			String passwordData=rs.getString("password");
			Date createdateData=rs.getDate("create_date");
			return new UserDataBeans(id,emailData,nameData,codeData,addressData,passwordData,createdateData);
		}catch(SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public UserDataBeans userInfo(int userid) {
		Connection con=null;
		try {
			con=DBManager.getConnection();

			String sql="SELECT*FROM user WHERE id=?";

			PreparedStatement st=con.prepareStatement(sql);
			st.setInt(1, userid);
			ResultSet rs=st.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int id=rs.getInt("id");
			String emailData=rs.getString("email");
			String nameData=rs.getString("name");
			String codeData=rs.getString("code");
			String addressData=rs.getString("address");
			String passwordData=rs.getString("password");
			Date createdateData=rs.getDate("create_date");
			return new UserDataBeans(id,emailData,nameData,codeData,addressData,passwordData,createdateData);
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void userCreate(String email,String password,String password2,String name,String code,String address) {
		Connection con=null;
		try {
			con=DBManager.getConnection();
			String source=password;
    		Charset charset = StandardCharsets.UTF_8;
    		String algorithm = "MD5";
    		byte[] bytes;

    		bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String pass = DatatypeConverter.printHexBinary(bytes);

			String sql="INSERT INTO user(email,name,code,address,password,create_date,update_date)VALUES(?,?,?,?,?,now(),now())";

			PreparedStatement st=con.prepareStatement(sql);
			st.setString(1, email);
			st.setString(2, name);
			st.setString(3, code);
			st.setString(4, address);
			st.setString(5, pass);

			int result=st.executeUpdate();

		}catch(SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public UserDataBeans searchUserByEmail(String email) {
		Connection con=null;
		try {
			con=DBManager.getConnection();

			String sql="SELECT*FROM user WHERE email=?";

			PreparedStatement st=con.prepareStatement(sql);
			st.setString(1, email);
			ResultSet rs=st.executeQuery();

			if(!rs.next()) {
				return null;
			}
			String Email=rs.getString("email");
			return new UserDataBeans();
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void userUpdate(int id,String email,String code,String address,String password,String password2) {
		Connection con=null;
		try {
			con=DBManager.getConnection();
			String source=password;
    		Charset charset = StandardCharsets.UTF_8;
    		String algorithm = "MD5";
    		byte[] bytes;

    		bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String pass = DatatypeConverter.printHexBinary(bytes);
			String sql="UPDATE user SET email=?,code=?,address=?,password=? where id=?";
			PreparedStatement st=con.prepareStatement(sql);

			st.setString(1, email);
			st.setString(2, code);
			st.setString(3, address);
			st.setString(4, pass);
			st.setInt(5, id);

			int result=st.executeUpdate();
		}catch(SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
