package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.ItemBeans;

public class itemDataDao {


	public static ArrayList<ItemBeans> getRandomItem(int limit){
		Connection con=null;
		PreparedStatement st=null;
		try {
			con=DBManager.getConnection();

			st=con.prepareStatement("SELECT*FROM item ORDER BY RAND() LIMIT ?");
			st.setInt(1, limit);

			ResultSet rs=st.executeQuery();
			ArrayList<ItemBeans> itemList=new ArrayList<ItemBeans>();

			while (rs.next()) {
				ItemBeans item=new ItemBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setCategory_id(rs.getInt("category_id"));
				item.setPrice(rs.getInt("price"));
				item.setItem_img_name(rs.getString("item_img_name"));
				item.setDescription(rs.getString("description"));
				item.setScene_img_name(rs.getString("scene_img_name"));
				item.setDetail_img_name(rs.getString("detail_img_name"));

				itemList.add(item);
			}
			return itemList;
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}finally {
			if(con!=null){
				try {
				con.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}

		}
	}

	public static ArrayList<ItemBeans> getItemByName(String searchword){
		Connection con = null;
		PreparedStatement st = null;
		try {
			con=DBManager.getConnection();

			st=con.prepareStatement("SELECT*FROM item WHERE name LIKE ?");

			st.setString(1, "%"+searchword+"%");
			ResultSet rs = st.executeQuery();

			ArrayList<ItemBeans> itemList=new ArrayList<ItemBeans>();

			while(rs.next()) {
				ItemBeans item=new ItemBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setCategory_id(rs.getInt("category_id"));
				item.setPrice(rs.getInt("price"));
				item.setItem_img_name(rs.getString("item_img_name"));
				item.setDescription(rs.getString("description"));
				item.setScene_img_name(rs.getString("scene_img_name"));
				item.setDetail_img_name(rs.getString("detail_img_name"));

				itemList.add(item);

			}
			return itemList;
		}catch(SQLException e) {
			return null;
		}finally {
			if(con!=null){
				try {
				con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
		}
	  }
	}

	public static ArrayList<ItemBeans> getItemByCategoryId(int id){
		Connection con = null;
		PreparedStatement st = null;
		try {
			con=DBManager.getConnection();

			st=con.prepareStatement("SELECT*FROM item WHERE category_id=?");
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();

			ArrayList<ItemBeans> itemList=new ArrayList<ItemBeans>();

			while(rs.next()) {
				ItemBeans item=new ItemBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setCategory_id(rs.getInt("category_id"));
				item.setPrice(rs.getInt("price"));
				item.setItem_img_name(rs.getString("item_img_name"));
				item.setDescription(rs.getString("description"));
				item.setScene_img_name(rs.getString("scene_img_name"));
				item.setDetail_img_name(rs.getString("detail_img_name"));

				itemList.add(item);


		    }
	        return itemList;
		}catch(SQLException e) {
			return null;
        }

    }

	public static ArrayList<ItemBeans> getItemByImg(int id){
		Connection con = null;
		PreparedStatement st = null;
		try {
			con=DBManager.getConnection();

			st=con.prepareStatement("SELECT*FROM item WHERE id=?");
			st.setInt(1, id);
			ResultSet rs=st.executeQuery();

			ArrayList<ItemBeans> itemList=new ArrayList<ItemBeans>();

			while(rs.next()) {
				ItemBeans item=new ItemBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setCategory_id(rs.getInt("category_id"));
				item.setPrice(rs.getInt("price"));
				item.setItem_img_name(rs.getString("item_img_name"));
				item.setDescription(rs.getString("description"));
				item.setScene_img_name(rs.getString("scene_img_name"));
				item.setDetail_img_name(rs.getString("detail_img_name"));

				itemList.add(item);


		    }
			return itemList;
		}catch(SQLException e) {
			return null;
		}
	}

	public static ItemBeans itemdetail(int id) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con=DBManager.getConnection();

			st=con.prepareStatement("SELECT*FROM item WHERE id=?");
			st.setInt(1, id);
			ResultSet rs=st.executeQuery();

			ItemBeans item=new ItemBeans();
			if(rs.next()) {
		    item.setId(rs.getInt("id"));
		    item.setCategory_id(rs.getInt("category_id"));
			item.setName(rs.getString("name"));
			item.setPrice(rs.getInt("price"));
			item.setItem_img_name(rs.getString("item_img_name"));
			item.setScene_img_name(rs.getString("scene_img_name"));
			item.setDescription(rs.getString("description"));
			item.setDetail_img_name(rs.getString("detail_img_name"));
			}

			return item;



		}catch(SQLException e) {
			return null;
		}finally {
			if (con != null) {
	          	try {
	                		con.close();
	            	} catch (SQLException e) {
	                		e.printStackTrace();
	                		return null;
	            	}
		         }
		}
	}

	public void createCart(int itemid,int userid) {
		Connection con=null;
		PreparedStatement st=null;
		try {
			con=DBManager.getConnection();

			st=con.prepareStatement("INSERT INTO cart (item_id,user_id)VALUES(?,?)");
			st.setInt(1, itemid);
			st.setInt(2, userid);

			int result=st.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static ArrayList<ItemBeans> getCart(int userid) {
		Connection con=null;
		PreparedStatement st=null;
		try {
			con=DBManager.getConnection();

			st=con.prepareStatement(
					"SELECT*FROM cart"
					+" JOIN item"
					+" ON item.id=item_id"
					+" WHERE user_id=?");
			st.setInt(1, userid);
			ResultSet rs=st.executeQuery();

			ArrayList<ItemBeans> itemList=new ArrayList<ItemBeans>();
			while(rs.next()) {
				ItemBeans item=new ItemBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setCategory_id(rs.getInt("category_id"));
				item.setPrice(rs.getInt("price"));
				item.setItem_img_name(rs.getString("item_img_name"));
				item.setDescription(rs.getString("description"));
				item.setScene_img_name(rs.getString("scene_img_name"));
				item.setDetail_img_name(rs.getString("detail_img_name"));


				itemList.add(item);
			}return itemList;

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void DeleteItemFromCartbyItemId(int id) {
		Connection con=null;
		PreparedStatement st=null;
		try {
			con=DBManager.getConnection();

			st=con.prepareStatement("DELETE FROM cart WHERE id=?");

			st.setInt(1, id);
			int result=st.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void AddItem(String name,String category_id,String price,String item_img_name,String description,String scene_img_name,String detail_img_name) {
		Connection con=null;
		PreparedStatement st=null;
		try {
			con=DBManager.getConnection();

			st=con.prepareStatement("INSERT INTO item(name,category_id,price,item_img_name,description,scene_img_name,detail_img_name)VALUES(?,?,?,?,?,?,?)");

			st.setString(1, name);
			st.setString(2, category_id);
			st.setString(3, price);
			st.setString(4, item_img_name);
			st.setString(5, description);
			st.setString(6, scene_img_name);
			st.setString(7, detail_img_name);


			int result=st.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void UpdateItem(String name,String category_id,String price,String item_img_name,String description,String scene_img_name,String detail_img_name,int id) {
		Connection con=null;
		PreparedStatement st=null;
		try {
			con=DBManager.getConnection();

			st=con.prepareStatement("UPDATE item SET name=?,category_id=?,price=?,item_img_name=?,description=?,scene_img_name=?,detail_img_name=? WHERE id=?");
			st.setString(1, name);
			st.setString(2, category_id);
			st.setString(3, price);
			st.setString(4, item_img_name);
			st.setString(5, description);
			st.setString(6, scene_img_name);
			st.setString(7, detail_img_name);
			st.setInt(8, id);

			int result=st.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void DeleteItem(int id) {
		Connection con=null;
		PreparedStatement st=null;
		try {
			con=DBManager.getConnection();

			st=con.prepareStatement("DELETE FROM item WHERE id=?");
			st.setInt(1, id);

			int result=st.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void DeleteCartByUserId(int userid) {
		Connection con=null;
		PreparedStatement st=null;
		try {
			con=DBManager.getConnection();

			st=con.prepareStatement("DELETE FROM cart where user_id=?");
			st.setInt(1, userid);

			int result=st.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
