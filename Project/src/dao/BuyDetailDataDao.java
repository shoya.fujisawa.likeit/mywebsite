package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.BuyDataBeans;



public class BuyDetailDataDao{
	public void BuyDetailData(int user_id,String total_price) {
		Connection con=null;
		try {
			con=DBManager.getConnection();

			String sql="INSERT INTO buy(user_id,total_price,buy_date)VALUES(?,?,now())";
			PreparedStatement st=con.prepareStatement(sql);

			st.setInt(1, user_id);
			st.setString(2, total_price);

			int result=st.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public BuyDataBeans getBuyId(int user_id) {
		Connection con=null;
		try {
			con=DBManager.getConnection();

			String sql="SELECT id FROM buy WHERE user_id=? ORDER by buy_date desc limit 1";
			PreparedStatement st=con.prepareStatement(sql);

			st.setInt(1, user_id);
			ResultSet rs=st.executeQuery();
			if(!rs.next()) {
				return null;
			}

			BuyDataBeans bdb=new BuyDataBeans();
			bdb.setId(rs.getInt("id"));



			return bdb;
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public ArrayList<BuyDataBeans> getBuyData(int buy_id) {
		Connection con=null;
		try {
			con=DBManager.getConnection();

			String sql="SELECT*FROM buy_history"
					+ " JOIN item"
					+ " ON item_id=item.id"
					+ " WHERE user_id=?";
			PreparedStatement st=con.prepareStatement(sql);
			st.setInt(1, buy_id);

			ResultSet rs=st.executeQuery();
			ArrayList<BuyDataBeans> BuyData=new ArrayList<BuyDataBeans>();

			while(rs.next()) {
				BuyDataBeans bd=new BuyDataBeans();
				bd.setId(rs.getInt("id"));
				bd.setUser_id(rs.getInt("user_id"));
				bd.setItem_id(rs.getInt("item_id"));
				bd.setPrice(rs.getInt("price"));
				bd.setName(rs.getString("name"));


				BuyData.add(bd);
			}
			return BuyData;
		}catch(SQLException e){
			e.printStackTrace();
			return null;
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void BuyHistoryData (int buyid,int userid) {
		Connection con=null;
		try {
			con=DBManager.getConnection();

			String sql="INSERT INTO buy_history (id,item_id,buy_id) SELECT id,item_id,? FROM cart WHERE user_id=?;";
			PreparedStatement st=con.prepareStatement(sql);
			st.setInt(1, buyid);
			st.setInt(2, userid);

			int result=st.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public ArrayList<BuyDataBeans> BuyHistory(int userid){
		Connection con=null;
		try {
			con=DBManager.getConnection();

			String sql="SELECT item.id,name,price,buy_date "
					  +"FROM buy_history "
					  +"JOIN item "
					  +"ON buy_history.item_id=item.id "
					  +"JOIN buy "
					  +"ON buy_history.buy_id=buy.id "
					  +"WHERE user_id=? "
					  +"ORDER BY buy_date DESC";
			PreparedStatement st=con.prepareStatement(sql);
			st.setInt(1, userid);

			ResultSet rs=st.executeQuery();
			ArrayList<BuyDataBeans> BuyData=new ArrayList<BuyDataBeans>();

			while(rs.next()) {
				BuyDataBeans bdb=new BuyDataBeans();
				bdb.setItem_id(rs.getInt("item.id"));
				bdb.setName(rs.getString("name"));
				bdb.setPrice(rs.getInt("price"));
				bdb.setBuy_date(rs.getDate("buy_date"));


				BuyData.add(bdb);
			}
			return BuyData;


		}catch(SQLException e){
			e.printStackTrace();
			return null;
		}finally {
			if(con!=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}




}
