package eccontroller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import dao.itemDataDao;

/**
 * Servlet implementation class additem
 */
@WebServlet("/additem")
@MultipartConfig(location="/Users/shoya/Documents/Git/mywebsite/Project/WebContent/image",maxFileSize=10000000)
public class additem extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public additem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/additem.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String name=request.getParameter("item_name");
		String categoryId=request.getParameter("category_id");
		String price=request.getParameter("price");

		Part item_pic=request.getPart("item_pic");
		String ipName=this.getFileName(item_pic);
		item_pic.write(ipName);

		String description=request.getParameter("item_description");

		Part itemscene_pic=request.getPart("itemscene_pic");
		String ispName=this.getFileName(itemscene_pic);
		itemscene_pic.write(ispName);

		Part itemdetail_pic=request.getPart("itemdetail_pic");
		String idpName=this.getFileName(itemdetail_pic);
		itemdetail_pic.write(idpName);

		itemDataDao itemDataDao=new itemDataDao();

		if(name.equals("")||categoryId.equals("")||price.equals("")||item_pic.equals(null)||description.equals(null)||itemscene_pic.equals(null)||itemdetail_pic.equals("")) {

			request.setAttribute("errMsg", "未入力項目があります");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/additem.jsp");
			dispatcher.forward(request, response);
			return;
		}
		itemDataDao.AddItem(name, categoryId, price, ipName, description, ispName, idpName);
		response.sendRedirect("top");
	}

	private String getFileName(Part part) {
		String name=null;
		for(String dispotion:part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
		}
		return name;
	}

}
