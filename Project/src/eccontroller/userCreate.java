package eccontroller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserDataBeans;
import dao.UserDataDao;

/**
 * Servlet implementation class userCreate
 */
@WebServlet("/userCreate")
public class userCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public userCreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String name=request.getParameter("name");
		String email=request.getParameter("mail_address");
		String password=request.getParameter("password");
		String password2=request.getParameter("password-confirm");
		String code=request.getParameter("code");
		String address=request.getParameter("address");

		UserDataDao userDataDao=new UserDataDao();

		UserDataBeans user=userDataDao.searchUserByEmail(email);
		if(user!=null) {
			request.setAttribute("userName", name);
			request.setAttribute("userCode", code);
			request.setAttribute("userAddress", address);
			request.setAttribute("errMsg2", "このメールアドレスはすでに使われています");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);

			return;
		}

		if(!(password.equals(password2))) {
			request.setAttribute("userName", name);
			request.setAttribute("userCode", code);
			request.setAttribute("userAddress", address);
			request.setAttribute("errMsg2", "入力内容が異なります");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);

			return;
		}

		if(name.equals("")||email.equals("")||password.equals("")||password2.equals("")||code.equals("")||address.equals("")) {
			request.setAttribute("userName", name);
			request.setAttribute("userCode", code);
			request.setAttribute("userAddress", address);
			request.setAttribute("errMsg2", "未入力項目があります");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);

			return;
		}

		userDataDao.userCreate(email, password, password2, name, code, address);
		response.sendRedirect("top");
	}

}
