package eccontroller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemBeans;
import dao.itemDataDao;

/**
 * Servlet implementation class item
 */
@WebServlet("/item")
public class item extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public item() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			int id=Integer.parseInt(request.getParameter("id"));
			itemDataDao itemDataDao=new itemDataDao();
			ItemBeans itemList=itemDataDao.itemdetail(id);

			request.setAttribute("itemList", itemList);

			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/item.jsp");
			dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			String searchword=request.getParameter("searchword");
			ArrayList<ItemBeans> itemSearchResult=itemDataDao.getItemByName(searchword);

			request.setAttribute("itemList", itemSearchResult);
			request.setAttribute("searchword", searchword);

			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/itemlist.jsp");
			dispatcher.forward(request, response);

		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
