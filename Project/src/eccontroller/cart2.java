package eccontroller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserDataBeans;
import dao.itemDataDao;

/**
 * Servlet implementation class cart2
 */
@WebServlet("/cart2")
public class cart2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public cart2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDataBeans user=(UserDataBeans)session.getAttribute("userInfo");
		if(user==null) {
			request.setAttribute("errMsg", "ログイン情報がありません");
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
			dispatcher.forward(request, response);

			return;
		}
		int userId=user.getId();
		itemDataDao itemDataDao=new itemDataDao();
		ArrayList<ItemBeans> itemList=itemDataDao.getCart(userId);
		request.setAttribute("itemList", itemList);
		request.setAttribute("listCount", itemList.size());
		if(itemList.size()==0) {
			request.setAttribute("errMsg", "カートに商品がありません");
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
			dispatcher.forward(request, response);

			return;
		}


		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
