package eccontroller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.UserDataBeans;
import dao.BuyDataDao;
import dao.BuyDetailDataDao;
import dao.itemDataDao;

/**
 * Servlet implementation class buy
 */
@WebServlet("/buy")
public class buy extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public buy() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		UserDataBeans user=(UserDataBeans)session.getAttribute("userInfo");
		int userId=user.getId();

		/*HttpSession session2=request.getSession();
		ItemBeans item=(ItemBeans)session2.getAttribute("itemList");
		int itemId=item.getId();*/

		HttpSession session2=request.getSession();
		String tp=(String)session2.getAttribute("tp");

		BuyDetailDataDao BuyDetailDataDao=new BuyDetailDataDao();

		BuyDetailDataDao.BuyDetailData(userId, tp);

		BuyDataBeans bdb=BuyDetailDataDao.getBuyId(userId);
		request.setAttribute("bddb", bdb);
		int buyId=bdb.getId();

	    BuyDetailDataDao.BuyHistoryData(buyId, userId);

		session2.removeAttribute("tp");

		itemDataDao itemDataDao=new itemDataDao();
		itemDataDao.DeleteCartByUserId(userId);

		BuyDataDao BuyDataDao=new BuyDataDao();
		BuyDataBeans bd=BuyDataDao.getBuyData(buyId);
		request.setAttribute("bd", bd);

		ArrayList <BuyDataBeans> BuyList=BuyDataDao.getBuyDataDetail(buyId);
		request.setAttribute("BuyList", BuyList);





		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/buy.jsp");
		dispatcher.forward(request, response);


		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
