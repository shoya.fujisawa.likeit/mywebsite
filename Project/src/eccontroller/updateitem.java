package eccontroller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemBeans;
import dao.itemDataDao;

/**
 * Servlet implementation class updateitem
 */
@WebServlet("/updateitem")
public class updateitem extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public updateitem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id=Integer.parseInt(request.getParameter("id"));
		itemDataDao itemDataDao=new itemDataDao();
		ItemBeans itemList=itemDataDao.itemdetail(id);
		request.setAttribute("itemList", itemList);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/updateitem.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		int id=Integer.parseInt(request.getParameter("id"));
		String name=request.getParameter("item_name");
		String categoryId=request.getParameter("category_id");
		String price=request.getParameter("price");
		String item_pic=request.getParameter("item_pic");
		String description=request.getParameter("item_description");
		String itemscene_pic=request.getParameter("itemscene_pic");
		String itemdetail_pic=request.getParameter("itemdetail_pic");

		itemDataDao itemDataDao=new itemDataDao();
		itemDataDao.UpdateItem(name, categoryId, price, item_pic, description, itemscene_pic, itemdetail_pic, id);

		response.sendRedirect("top");
	}

}
