package eccontroller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserDataBeans;
import dao.itemDataDao;

/**
 * Servlet implementation class buyconfirm
 */
@WebServlet("/buyconfirm")
public class buyconfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public buyconfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDataBeans user=(UserDataBeans)session.getAttribute("userInfo");
		int userId=user.getId();

		ArrayList<ItemBeans> itemList=itemDataDao.getCart(userId);
		session.setAttribute("itemList", itemList);

		ItemBeans ib=new ItemBeans();
		String TotalItemPrice=ib.getTotalItemPrice(itemList);
	    session.setAttribute("tp", TotalItemPrice);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/buyconfirm.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
