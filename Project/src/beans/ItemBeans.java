package beans;

import java.io.Serializable;
import java.util.ArrayList;

public class ItemBeans implements Serializable{
	private int id;
	private String name;
	private int category_id;
	private int price;
	private String item_img_name;
	private String description;
	private String scene_img_name;
	private String detail_img_name;
	private int TotalItemPrice;

	public ItemBeans() {

	}

	public ItemBeans(int id,String name,int price,String item_img_name,String description,String scene_img_name,String detail_img_name) {
		this.id=id;
		this.name=name;
		this.price=price;
		this.item_img_name=item_img_name;
		this.description=description;
		this.scene_img_name=scene_img_name;
		this.detail_img_name=detail_img_name;
	}




	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getItem_img_name() {
		return item_img_name;
	}
	public void setItem_img_name(String item_img_name) {
		this.item_img_name = item_img_name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getScene_img_name() {
		return scene_img_name;
	}
	public void setScene_img_name(String scene_img_name) {
		this.scene_img_name = scene_img_name;
	}
	public String getDetail_img_name() {
		return detail_img_name;
	}
	public void setDetail_img_name(String detail_img_name) {
		this.detail_img_name = detail_img_name;
	}
	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}
	public String getTotalPrice() {
		return String.format("%,d", 350+this.price);
	}
	public String getTotalItemPrice(ArrayList<ItemBeans> items) {
		int total = 0;
		for (ItemBeans item : items) {
			total += item.getPrice();
		}
		return String.format("%,d", total+350);
	}
	public void setTotalItemPrice(int TotalItemPrice) {
		this.TotalItemPrice=TotalItemPrice;
	}

}
