package beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BuyDataBeans implements Serializable{
	private int id;
	private int user_id;
	private int item_id;
	private String total_price;
	private Date buy_date;
	private int price;
	private String name;

	public BuyDataBeans() {

	}

	public BuyDataBeans(int id) {
		this.id=id;
	}

	public BuyDataBeans(int id,int user_id,int item_id,String total_price,Date buy_date,int price,String name) {
		this.id=id;
		this.user_id=user_id;
		this.item_id=item_id;
		this.total_price=total_price;
		this.buy_date=buy_date;
		this.price=price;
		this.name=name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getItem_id() {
		return item_id;
	}

	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}

	public String getTotal_price() {
		return total_price;
	}

	public void setTotal_price(String total_price) {
		this.total_price = total_price;
	}

	public Date getBuy_date() {
		return buy_date;
	}

	public void setBuy_date(Date buy_date) {
		this.buy_date = buy_date;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}

	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(buy_date);
	}


}

